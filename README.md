package gunel;

public class Student {

	private String name;
	private String Major;
	private String email;
	private int nCredits;
	private double GPA;
	public Student (String name, String Major, String email, int nCredits, double GPA){
		this.name=name;
		this.Major=Major;
		this.email=email;
		this.nCredits=nCredits;
		this.GPA=GPA;
	}
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getMajor(){
		return Major;
	}
	public void setMajor(String Major){
		this.Major=Major;
	}
	public String getEmail(){
		return email;
	}
	public void setEmail(String email){
		this.email=email;
	}
	public int getNCredits(){
		return nCredits;
	}
	public void setNCredits(int nCredits){
		this.nCredits=nCredits;
	}
	public double getGPA(){
		return GPA;
	}
	public void setGPA(double GPA){
		this.GPA=GPA;
	}
	public void printInfo(){
		System.out.println("h1:" + getName());
		System.out.println(" your Major is: " + getMajor());
		System.out.println(" your email is: " + getEmail());
		System.out.println(" your nCredits is: " + getNCredits());
		System.out.println( " your GPA is: " + getGPA());
	
	}
	
		public static void main(String[] args) {
			String teacher= "engineer";
			String gunelq42 = "gunelq42";
			Student st=new Student (" Gunel " , teacher, gunelq42, 19 , 4 );
			st.printInfo();

}

}